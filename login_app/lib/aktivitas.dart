import 'package:flutter/material.dart';
import 'package:login_app/home_page.dart';
import 'package:login_app/riwayat.dart';

class BalikAktivitas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightGreen[500],
          title: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => HomePage()),
                          );
                        },
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("Aktivitas",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        body: AktivitasPage(),
      ),
    );
  }
}

class AktivitasPage extends StatefulWidget {
  @override
  _Aktivitas createState() => _Aktivitas();
}

class _Aktivitas extends State<AktivitasPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
            child: Text("Hari ini",
                style: TextStyle(
                    color: Colors.lightGreen[500],
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold)),
          ),
          _aktivitasnull(),
          _lihatriwayat()
        ],
      ),
    );
  }

  _aktivitasnull() {
    return Column(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
          child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0.0)),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: 40.0,
                        height: 40.0,
                        child: CircleAvatar(
                          child: Icon(
                            Icons.restore_from_trash,
                            size: 25.0,
                            color: Colors.black,
                          ),
                          backgroundColor: Colors.green[300].withOpacity(0.7),
                        ),
                      ),
                      SizedBox(width: 20.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Angkut Cepat Sedang Berjalanan",
                              style: TextStyle(
                                  color: Colors.red[300],
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold)),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text("21 Juli 2020   08.41",
                              style: TextStyle(
                                  color: Colors.red[300], fontSize: 10.0)),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  _lihatriwayat() {
    return Container(
      child: Row(
        children: <Widget>[
          SizedBox(
            height: 10.0,
            width: 20.0,
            child: FloatingActionButton(
              heroTag: 'btn2',
              backgroundColor: Colors.white,
              child: Icon(
                Icons.search,
                size: 0.0,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return BalikRiwayat();
                }));
              },
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Lihat Riwayat",
                  style: TextStyle(
                      color: Colors.lightBlue[300],
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold)),
              SizedBox(
                height: 10.0,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
