import 'package:flutter/material.dart';
import 'package:login_app/aktivitas.dart';
import 'package:login_app/home_page.dart';

class BalikBerlangganan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightGreen[500],
          title: Column(
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => HomePage()),
                          );
                        },
                      ),
                    ),
                    SizedBox(width: 20.0),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("Angkut Berlangganan",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        body: Berlangganan(),
      ),
    );
  }
}

class Berlangganan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Center(
        child: Column(
          children: [
            TextField(
              decoration: new InputDecoration(
                  hintText: "Nama",
                  labelText: "Nama",
                  icon: Icon(Icons.people)),
            ),
            TextField(
              decoration: new InputDecoration(
                  hintText: "Alamat",
                  labelText: "Alamat",
                  icon: Icon(Icons.add_location)),
            ),
            TextField(
              decoration: new InputDecoration(
                  hintText: "No Telepon",
                  labelText: "No Telepon",
                  icon: Icon(Icons.phone)),
            ),
            TextField(
              decoration: new InputDecoration(
                  hintText: "Catatan",
                  labelText: "Catatan",
                  icon: Icon(Icons.note_add)),
            ),
            TextField(
              decoration: new InputDecoration(
                  hintText: "Paket Berlangganan",
                  labelText: "Pilih paket",
                  icon: Icon(Icons.note_add)),
            ),
            TextField(
              decoration: new InputDecoration(
                  hintText: "Metode Pembayaran",
                  labelText: "Ovo/Tunai",
                  icon: Icon(Icons.add_location)),
            ),
            RaisedButton(
              child: Text(
                "Angkut Berlangganan",
                style: TextStyle(color: Colors.black),
              ),
              color: Colors.lightGreen[300],
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return BalikAktivitas();
                }));
              },
            ),
          ],
        ),
      ),
    );
  }
}
