import 'package:flutter/material.dart';
import 'package:login_app/angkutcepat.dart';
import 'package:login_app/angkutberlangganan.dart';
import 'package:login_app/aktivitas.dart';
import 'package:login_app/pembayaran.dart';

class BerandaPage extends StatefulWidget {
  static String tag = 'beranda-page';

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //CODE BARU YANG DITAMBAHKAN
      body: Container(
        child: Column(children: <Widget>[
          SizedBox(height: 70.0),
          Container(
            height: 300.0,
            child: GridView(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 2,
                    mainAxisSpacing: 20),
                children: <Widget>[
                  _gridItem(),
                  _gridItem2(),
                  _gridItem3(),
                  _gridItem4(),
                ]),
          ),
        ]),
      ),
    );
  }

  _gridItem() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 60.0,
            width: 60.0,
            child: FloatingActionButton(
              heroTag: 'btn1',
              backgroundColor: Colors.green[300].withOpacity(0.7),
              child: Icon(
                Icons.restore_from_trash,
                size: 25.0,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => BalikCepat()),
                );
              },
            ),
          ),
          SizedBox(height: 10.0),
          Text("Angkut Cepat",
              style: TextStyle(fontSize: 12.0, color: Colors.black)),
        ],
      ),
    );
  }

  _gridItem2() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 60.0,
            width: 60.0,
            child: FloatingActionButton(
              heroTag: 'btn2',
              backgroundColor: Colors.green[300].withOpacity(0.7),
              child: Icon(
                Icons.refresh,
                size: 20.0,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return BalikBerlangganan();
                }));
              },
            ),
          ),
          SizedBox(height: 10.0),
          Text("Angkut Berlangganan",
              style: TextStyle(fontSize: 12.0, color: Colors.black)),
        ],
      ),
    );
  }

  _gridItem3() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 60.0,
            width: 60.0,
            child: FloatingActionButton(
              heroTag: 'btn3',
              backgroundColor: Colors.green[300].withOpacity(0.7),
              child: Icon(
                Icons.assignment,
                size: 25.0,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return BalikAktivitas();
                }));
              },
            ),
          ),
          SizedBox(height: 10.0),
          Text("Aktivitas",
              style: TextStyle(fontSize: 12.0, color: Colors.black)),
        ],
      ),
    );
  }

  _gridItem4() {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 60.0,
            width: 60.0,
            child: FloatingActionButton(
              heroTag: 'btn4',
              backgroundColor: Colors.green[300].withOpacity(0.7),
              child: Icon(
                Icons.account_balance_wallet,
                size: 25.0,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return BalikPembayaran();
                }));
              },
            ),
          ),
          SizedBox(height: 10.0),
          Text("Pembayaran",
              style: TextStyle(fontSize: 12.0, color: Colors.black)),
        ],
      ),
    );
  }
}
