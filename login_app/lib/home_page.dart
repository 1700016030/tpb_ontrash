import 'package:flutter/material.dart';
import './beranda_page.dart' as berandapage;
import './aktivitas.dart' as aktivitas;
import './pembayaran.dart' as pembayaran;
import './profil.dart' as profil;

class HomePage extends StatefulWidget {
  static String tag = 'home-page';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  //create controller untuk tabBar
  TabController controller;
  @override
  void initState() {
    controller = new TabController(vsync: this, length: 4);
    //tambahkan SingleTickerProviderStateMikin pada class _HomeState
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      //create appBar
      appBar: new AppBar(
        //warna background
        backgroundColor: Colors.lightGreen,
        //judul
        title: new Text("On-Trash"),
        //bottom
        bottom: new TabBar(
          controller: controller,
          tabs: <Widget>[
            new Tab(
              icon: new Icon(Icons.home),
              text: "Beranda",
            ),
            new Tab(
              icon: new Icon(Icons.assignment),
              text: "Aktivitas",
            ),
            new Tab(
              icon: new Icon(Icons.account_balance_wallet),
              text: "Pembayaran",
            ),
            new Tab(
              icon: new Icon(Icons.account_circle),
              text: "Profil",
            ),
          ],
        ),
      ),

      //source code lanjutan
      //buat body untuk tab viewnya
      body: new TabBarView(
        //controller untuk tab bar
        controller: controller,
        children: <Widget>[
          //kemudian panggil halaman sesuai tab yang sudah dibuat
          new berandapage.BerandaPage(),
          new aktivitas.AktivitasPage(),
          new pembayaran.Pembayaran(),
          new profil.Profil()
        ],
      ),
    );
  }
}
